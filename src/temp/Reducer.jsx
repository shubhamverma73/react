import React, { useState, useReducer } from 'react';

/* =================== Normal way with useState =============================
const Reducer = () => {
    const[count, setCount] = useState(0);
    return (
        <>
            <h1>React Reducer {count}</h1>
            <button onClick={ () => setCount(count + 1) } >Increase</button>
            <button onClick={ () => setCount(count - 1) } >Decrease</button>
        </>
    );
}
=================== Normal way with useState ============================= */

const initialState = 0;

function reducer(state, action) {
    switch (action.type) {
        case 'increment':
            return state + 1;
        case 'decrement':
            return state - 1;
        default:
            throw new Error();
    }
}

const Reducer = () => {

    const [state, dispatch] = useReducer(reducer, initialState);

    return (
        <>
            <h1>React Reducer {state}</h1>
            <button onClick={() => dispatch({type: 'increment'})} >Increase</button>
            <button onClick={() => dispatch({type: 'decrement'})} >Decrease</button>
        </>
    );
}

export default Reducer;