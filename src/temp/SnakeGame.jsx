import React from 'react';

const Memo = () => {

    var rows = [];
    
    var k = 100;
    for (var i = 1; i <= 10; i++) {
        rows.push(
            '<div class="row">'
        );
        for (var j = 1; j <= 10; j++) {
            rows.push(
                `<div class="items">${k}</div>`
            );
            k--;
        }
        rows.push(
            '</div>'
        );
    }

    var snakeData = rows.join(''); //converting array into string

    return (
        <>
            <div className="content">                
                <div dangerouslySetInnerHTML={{__html: snakeData}} />
            </div>
        </>
    );
}

export default Memo;