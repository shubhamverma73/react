import React, { useState, useEffect } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';

const PaginationWithFilter = () => {

    const { SearchBar } = Search;
    const[user, setUser] = useState([]);
    const headerSortingStyle = { backgroundColor: '#c8e6c9' };
    const columns = [
        { dataField: 'id', text: 'Product ID', sort: true, headerSortingStyle },
        { dataField: 'name', text: 'Product Name', sort: true },
        { dataField: 'username', text: 'Product Price', sort: true }
    ];

    const products = [
        { id: '1', name: 'shubham', username: 'shubh' },
        { id: '2', name: 'sonu', username: 'shubh' },
        { id: '3', name: 'vishwash', username: 'shubh' },
        { id: '4', name: 'shweta', username: 'shubh' },
        { id: '5', name: 'shwati', username: 'shubh' },
        { id: '6', name: 'megha', username: 'shubh' },
        { id: '7', name: 'mona', username: 'shubh' },
        { id: '8', name: 'deepak', username: 'shubh' },
        { id: '9', name: 'dipu', username: 'shubh' },
        { id: '10', name: 'saumya', username: 'shubh' },
        { id: '11', name: 'arti', username: 'shubh' },
    ];

    const storesData = async (data) => {
        let result = await fetch("https://jsonplaceholder.typicode.com/users", {
            method: "GET",
        });
        result = await result.json();
        console.log(result);
        setUser(result);
    }
    useEffect(() => {
        storesData();
    }, []);

    return (
        <>

            <ToolkitProvider
            keyField="id"
            data={ products }
            columns={ columns }
            search
            >
            {
                props => (
                <div>
                    <h3>Input something at below input field:</h3>
                    <SearchBar { ...props.searchProps }
                    className="input-group pull-right"
                    style={ { color: 'black' } }
                    delay={ 1000 }
                    placeholder="Search Something!!!"
                    />
                    <hr />
                    <BootstrapTable
                    { ...props.baseProps }
                    pagination={ paginationFactory() }
                    hover
                    striped
                    condensed
                    noDataIndication="Table is Empty"
                    headerWrapperClasses="foo"
                    />
                </div>
                )
            }
            </ToolkitProvider>

        </>
    );
}

export default PaginationWithFilter;