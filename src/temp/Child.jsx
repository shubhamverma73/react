import React, { useEffect, useState } from 'react';

const Child = (props) => {
    const[child, setchild] = useState('Data from Child Component');
    useEffect(() => {
        props.accessChildData(child);
    }, []);

    return (
        <>
            <h1>Child Component</h1>
        </>
    );
}
export default Child;