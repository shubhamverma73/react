import React, { useState, useEffect } from 'react';

const Json = () => {

    const[prdtData, setPrcData] = useState([]);
    const productData = async () => {
        let result = await fetch("https://api.spacexdata.com/v3/launches", {
            method: "GET",
            headers: {'Content-Type' : 'application/json'},
        });
        result = await result.json();
        setPrcData(result);
    }
    useEffect(() => {
        productData();
    }, []);

    var newArr = [];
    for(var i=0; i<prdtData.length; i++) {
        
        newArr.push(
            {   "flight_number": prdtData[i].flight_number,
                "details": prdtData[i].details,
                "core_serial": prdtData[i].rocket.first_stage.cores[0].core_serial}
        );
    }
    console.log(newArr);

    return (
        <>
            <div className="content">                
                
            </div>
        </>
    );
}

export default Json;