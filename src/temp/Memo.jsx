import React, { useState } from 'react';

const Memo = () => {

    const [count, setData] = useState(1);
    const [item, setItem] = useState(20);

    const multocount = React.useMemo(
        function appleTime() {
            console.warn("Hello")
            return 100 * count;
        }
    , [item]) //Here item is means, if we click on Update Item itan it will call either not

    return (
        <>
            <h1>Hooks in React {count}</h1>
            <h2>{multocount}</h2>
            <button onClick={() => setData(count + 1)}>Update State</button>
            <button onClick={() => setItem(item * 10)}>Update Item</button>
        </>
    );
}

export default Memo;