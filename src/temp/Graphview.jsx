import React from "react";
import Graph from "./Graph";

function Graphview() {

	var graphData = [
						{date: 0, value: 25	},
						{date: 1, value: 20	},
						{date: 2, value: 10	},
						{date: 3, value: 35	},
						{date: 4, value: 10	},
					];

  return (
    <div className="App">
	<br />
        <div>
            <Graph
                data={graphData}
                width={200}
                height={200}
                innerRadius={60}
                outerRadius={100}
            />
        </div>
    </div>
  );
}
export default Graphview;