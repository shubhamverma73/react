import React, { useState } from 'react';
import Child from './Child';

const ChildToParent = () => {
    const[parent, setParent] = useState();
    const accessChildData = (data) => {
        setParent(data);
    }
    return (
        <>
            <h1>Parent Component</h1>
            <h1>{parent}</h1>
            <Child accessChildData={accessChildData} />
        </>
    );
}
export default ChildToParent;