import React from 'react';
//import { SRLWrapper } from 'simple-react-lightbox';
import Lightboxes from "react-awesome-lightbox";
// You need to import the CSS only once
import "react-awesome-lightbox/build/style.css";

const Lightbox = () => {

    /*const elements = [
        {
          src: 'https://www.telegraph.co.uk/content/dam/business/spark/GEF/green-handshake-getty-683962670-2-xlarge.jpg',
          caption: 'Lorem ipsum dolor sit amet',
          width: 1920,
          height: 'auto'
        },
        {
          src: 'https://ichef.bbci.co.uk/news/976/cpsprodpb/3ED6/production/_106768061_gettyimages-1126199440.jpg',
          thumbnail: 'https://www.simple-react-lightbox.dev/docs/gallery/thumbnails/unsplash05.jpg',
          caption: 'Commodo commodo dolore',
          width: 1024,
          height: 'auto'
        },
        {
          src: 'https://www.pbs.org/wnet/nature/files/2019/07/Super-Hummingbirds-2-1280x675.jpg',
          thumbnail: 'https://www.simple-react-lightbox.dev/docs/gallery/thumbnails/unsplash05.jpg',
          caption: 'Vimeo video',
          autoplay: false,
          showControls: true
        }
    ]*/

    let images = [
        {
            url:"https://www.telegraph.co.uk/content/dam/business/spark/GEF/green-handshake-getty-683962670-2-xlarge.jpg",
            title:"image title 1"
        },
        {
            url:"https://ichef.bbci.co.uk/news/976/cpsprodpb/3ED6/production/_106768061_gettyimages-1126199440.jpg",
            title:"image title 2"
        }
    ];

    return (
        <>
            Test
            {/* <SRLWrapper elements={elements} />
            <SRLWrapper>
                <a href="https://www.simple-react-lightbox.dev/docs/gallery/thumbnails/unsplash05.jpg">
                    <img src="https://www.simple-react-lightbox.dev/docs/gallery/thumbnails/unsplash05.jpg" alt="Umbrella" />
                </a>
                <a href="https://www.simple-react-lightbox.dev/docs/gallery/thumbnails/unsplash05.jpg">
                    <img src="https://www.simple-react-lightbox.dev/docs/gallery/thumbnails/unsplash05.jpg" alt="Blue sky" />
                </a>
            </SRLWrapper> */}
            <Lightboxes image="https://www.simple-react-lightbox.dev/docs/gallery/thumbnails/unsplash05.jpg" title="Image Title"></Lightboxes>
            <Lightboxes images={images} />
        </>
    );
}

export default Lightbox;